package com.wings;

import java.util.Arrays;

public class MoveZeros {

	public static void main(String[] args) {
		
		int [] array = {12, 0, 7, 0, 8, 0, 3};
		int zeros = 0;
		int nonZeros = 0;
		System.out.println(array.length);
		for (int i=0;i<array.length;i++) {
			if(array[i]==0) {
				//array[count]=array[i];
				++zeros;
			}
			else {
				
				array[nonZeros]=array[i];
				++nonZeros;
			}
				
			
		}
		System.out.println(zeros+" "+nonZeros);
		while(nonZeros< array.length) {
			array[nonZeros]=0;
			nonZeros++;
		}
		System.out.println(Arrays.toString(array));
	}

}
