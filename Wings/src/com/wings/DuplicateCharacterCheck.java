package com.wings;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class DuplicateCharacterCheck {

	public static void main(String[] args) {
		
		//String name1 = "Adarsh";
		String name2 = "adarsh";
		int [] array1 = { 1,23,3,23,2,3,4,2};
		Set<Integer> uniqueElements = new HashSet<>();
        
	    Set<Integer> duplicateElements =  Arrays.stream(array1)
	                                            .filter(i -> !uniqueElements.add(i))
	                                            .boxed()
	                                            .collect(Collectors.toSet());
	         
	    System.out.println(duplicateElements);
		HashMap<Character,Integer> hm = new HashMap<>();
		char [] charArray = name2.toCharArray();
		System.out.println(charArray.length);
		for(int i=0;i<charArray.length;i++) {
			if(hm.containsKey(charArray[i]))
				hm.put(charArray[i], hm.get(charArray[i])+1);
			else
				hm.put(charArray[i],1);
		}
		Set<Character> hs=hm.keySet();
		hs.stream().sorted().forEach(x -> System.out.print(x));
		System.out.println(hm.keySet());
		for(char c: hm.keySet()) {
			if(hm.get(c)>0) {
				System.out.println(c+ " : "+hm.get(c));
			}
		}
		
	}

}
