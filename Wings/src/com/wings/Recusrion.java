package com.wings;

public class Recusrion {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		printReverse(new char[] {'1','2','e','r'});

	}


private static void printReverse(char [] str) {
	  helper(0, str);
	}

	private static void helper(int index, char [] str) {
	  if (str == null || index >= str.length) {
	    return;
	  }
	  helper(index + 1, str);
	  System.out.print(str[index]);
	}
}