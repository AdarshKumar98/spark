package com.wings;

public class reverseEachWord {

	public static void main(String[] args) {
		
		reverseWord("Tcs Experian Adarsh Dell acer google");
	}

	private static void reverseWord(String string) {
		String [] splitWords = string.split(" ");
		String reversedString ="";
		for (int i = 0; i<splitWords.length;i++) {
			String reverse ="";
			for(int j=splitWords[i].length()-1;j>=0;j--) {
		 		reverse = reverse +splitWords[i].charAt(j);
		 	}
			reversedString+=reverse+" ";
			 	
			
		}
		System.out.println(" Original  =  "+string);
		System.out.println(" Reversed  =  "+reversedString);
	}

}
