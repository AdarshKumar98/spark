package com.wings;

public class NumberSeries {

	public static void main(String[] args) {
		
		//0,0,7,6,14,12,21,18, 28		
		int odd = 0;
		int even  = 0;
		int n =8;
		for(int i =1;i<n;i++ ) {
			if(i%2 == 0)
				even+=6;
			else 
				odd+=7;
			
		}
		if(n%2 == 0) System.out.println(even);
		else System.out.println(odd);
	}

}
