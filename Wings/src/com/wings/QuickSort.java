package com.wings;

import java.util.Arrays;

public class QuickSort {

	public static void main(String[] args) {
		
		int [] array= {23,12,131,121,32,43,656,232,121,45,56,34,3232,2323,23};
		System.out.println("Unsorted Array  "+Arrays.toString(array));
		int result[] =quickSort(array,0,array.length-1);
		System.out.println(" Quick Sort Array  "+Arrays.toString(result));
	}
	
	public static int[] quickSort(int[] arr,int i,int j) {
		
		if(i<j) {
			int pi=partition(arr,i,j);
			quickSort(arr,i,pi-1);
			quickSort(arr,pi+1,j);
		}
		return arr;
	}

	private static int partition(int[] arr, int i, int j) {
		int pivot=arr[j];
		int pindex=i-1;
		for(int a=i;a<j;a++) {
			if(arr[a]<=pivot) {
				pindex+=1;
				int pVal=arr[pindex];
				int aVal=arr[a];
				
				arr[pindex]=aVal;
				arr[a]=pVal;
			}
		}
		int pVal=arr[pindex+1];
		arr[j]=pVal;
		arr[pindex+1]=pivot;
		return pindex+1;
	}
	

}
