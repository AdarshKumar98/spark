package com.wings;

public class GameOnLeaderBoard {

	public static void main(String[] args) {

		int no_of_persons=7;
		int leaderBoard [] = {100,100,50,40,40,20,10};
		int playedGames = 5;
		int scoresInGame [] = {120,5,25,50,120};
		RankingsOnScores(no_of_persons,leaderBoard,playedGames,scoresInGame);

	}

	private static void RankingsOnScores(int no_of_persons, int[] leaderBoard, int playedGames, int[] scoresInGame) {

		int ranks[] = new int[playedGames];
		
		for(int i=0;i<scoresInGame.length;i++) {
			int count = 0;
			for(int j=0;j<leaderBoard.length-1;j++) {
				int value =leaderBoard[j+1];
				if(scoresInGame[i] < leaderBoard[j] && value!=leaderBoard[j]) {
					
					count++;
				}
				else continue;
				if(j==leaderBoard.length-2)count++;
			}
			ranks[i]=count;
		}
		for(int i:ranks) {
			System.out.println(i);
		}
		
	}

}
