/*
 * package com.wings;
 * 
 * public class CheckMobileNumber {
 * 
 * public static void main(String[] args) { String number = "8309400235"; int a
 * =10,b=20; System.out.println(" Before a ="+a+"  b = "+b); a ^=b; b ^=a; a
 * ^=b; double num=4.3434; String
 * numD=String.valueOf(num).substring(String.valueOf(num).indexOf("."));
 * System.out.println(numD); System.out.println(" after a ="+a+"  b = "+b);
 * System.out.println(" No Of Digits in 1234567 "+Math.floor(Math.log10(1234567)
 * +1) + 1); System.out.println((11 & 1) == 0 ? "EVEN":"ODD" );
 * if(checkMobile(number) && number.length() == 10) {
 * System.out.println(" mobile number verified"); } else
 * System.out.println(" plase enter valid mobile number");
 * 
 * 
 * 
 * } static boolean checkMobile(String number) { try { Long.parseLong(number);
 * // as integer exceeds range , long is required } catch(Exception e) { return
 * false; } return true; }
 * 
 * }
 */
package com.wings;

import java.math.BigInteger;
 
class CheckMobileNumber {
    public static int gcd(int a, int b)
    {
        BigInteger b1 = BigInteger.valueOf(a);
        BigInteger b2 = BigInteger.valueOf(b);
        BigInteger gcd = b1.gcd(b2);
        return gcd.intValue();
    }
 
    public static long gcd(long a, long b)
    {
        BigInteger b1 = BigInteger.valueOf(a);
        BigInteger b2 = BigInteger.valueOf(b);
        BigInteger gcd = b1.gcd(b2);
        return gcd.longValue();
    }
 
    // Driver method
    public static void main(String[] args)
    {
        System.out.println(gcd(5, 3));
        System.out.println(" prime Or not(true/false)    ===>  "+BigInteger.valueOf(1235).isProbablePrime(1));
        System.out.println(gcd(10000000000L, 600000000L));
    }
}