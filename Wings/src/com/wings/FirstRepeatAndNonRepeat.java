package com.wings;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class FirstRepeatAndNonRepeat {

	public static void main(String[] args) {
		
		checkHere("perianx");
	}

	private static void checkHere(String string) {
		
		HashMap<Character,Integer> hm = new HashMap<Character,Integer>();
		int count = 0;
		char [] array = string.toCharArray();
		for(char c:array) {
			if(hm.containsKey(c)){
				hm.put(c,hm.get(c)+1);
			}else {
				hm.put(c, 1);
				
			}
		}
		//Set<Map.Entry<Character, Integer>> entry = hm.entrySet();
		//System.out.println(entry);
		System.out.println(hm.entrySet());
		for(Map.Entry<Character, Integer> entry :hm.entrySet()) {
			if(entry.getValue() > 1) {
				System.out.println(" Repeated Chars of  " +string+" is "+entry.getKey());
				count ++;
			}
		}
		if(count == 0)
			System.out.println(" No Repeated Characters");
		for(char i:array) {
			if(hm.get(i) > 1) {
				System.out.println(" First Repeated Character of " +string+" is "+i);
				break;
			}
			
		}
		for(char i:array) {
			if(hm.get(i) == 1) {
				System.out.println(" First Non Character of " +string+" is "+i);
				break;
			}
		}
				
			
		}
	}


