package com.wings;

public class IsBinaryOrNot {

	public static void main(String[] args) {
		System.out.println(binaryOrNot(1121011));
		
	  
	}
	static boolean binaryOrNot(int number) {
		int copyNumber =  number;
		boolean isBinary = true;
		while(copyNumber > 0) {
			int temp = copyNumber % 10 ;
			if(temp > 1) {isBinary =false; break;}
			else 
				copyNumber/=10;
		}
		return isBinary;
		
	}

}
