package com.Collections;

import java.util.ArrayList;

public class ArrayList1 {

	public static void main(String[] args) {
		
		ArrayList<String> arrayList = new ArrayList<String>();
		//System.out.println(arrayList.size());
		//arrayList.ensureCapacity(20);
		System.out.println(arrayList.size());
		arrayList.add("JAVA");arrayList.add("J2EE");arrayList.add("JSP");arrayList.add("JAVA");arrayList.add("SERVLETS");
		arrayList.add("JAVA");arrayList.add("STRUTS");
		System.out.println(arrayList.indexOf("JAVA"));
		System.out.println(arrayList.lastIndexOf("JAVA"));
		System.out.println(arrayList.contains("java"));
		Object [] obj = arrayList.toArray();
		for ( Object ob:obj)System.out.print(ob+" ");
		System.out.println("\n"+arrayList.get(4));
		arrayList.set(6, "Spring"); 
		System.out.println(arrayList);
		
 
	}

}
