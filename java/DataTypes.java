import java.lang.*;
public class DataTypes{
	public static void main(String[] args){
		// asigning values to primitive variables  ...
		
		int a=(int)Math.pow(2,32); // ty casting and int Max value 
		int minValue=-(int)Math.pow(2,32);
		short aa=1231;
		float b=2.24f;  // type casting 
		double c=-42937737.1d;
		char d='c';
		long e=(long)Math.pow(2,64); // type casting and long max value
		boolean f=true;
		 
		// type casting ..
		
		int charValue= d; // character to integer i.e ascii value 
        long floatValue= (long)b; // float to long ..
       	System.out.println(" byte max Value "+aa);	
		System.out.println(" ..Assigning Values ..\n");
		System.out.println(" Int max Value "+a);
		System.out.println(" Int min Value "+minValue);
		System.out.println(" Float  Value i.e b = "+b);
		System.out.println(" Double Value i.e c = "+c);
		System.out.println(" Character Value i.e d =  "+d);
		System.out.println(" Long Max Value i.e e = "+e);
		System.out.println(" Boolean Value "+f+"\n");
		System.out.println(" ...type casting..  \n");
		System.out.println(" Char to Int  "+charValue);
		System.out.println(" float to long  "+floatValue);
	}
}
		
		
		