import java.util.*;
public class Collections{
	public static void main(String [] args){
		List<Integer> intList=new ArrayList();
		// insertion order,allowing duplicates,does not allow null values,
		//throws null pointer Exception
		intList.add(10);intList.add(20);intList.add(30);
		intList.add(20);intList.add(40);intList.add(null);
		Set<Integer> intSet = new HashSet();
		//unordered,does not allow duplicates and null values;
		//throws java.lang.NullPointerException
		intSet.add(10);intSet.add(20);intSet.add(30);
		intSet.add(20);intSet.add(40);intSet.add(23);
		System.out.println(" Iterating List values ");
		try{for(int i:intList){
			System.out.print(i+" ");
		}}catch(Exception e){System.out.println(e);}
		System.out.println("\nIterating Set values ");
		try{for(int i:intSet){
		System.out.print(i+" "); }
		}catch (Exception e){System.out.print(e);}
		Map<Character,Integer> entries=new HashMap<Character,Integer>();
		entries.put(null,1);entries.put('b',null);entries.put('c',5);entries.put('d',6);
		entries.put('e',2);entries.put('f',4);entries.put('g',7);
		System.out.println("\n\nIterating Map Values ");
		for(Map.Entry<Character, Integer> entry : entries.entrySet()){
				System.out.print(entry.getKey()+": "+entry.getValue()+"  ");
		}
	}
}