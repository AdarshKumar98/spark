public class Classes{
	public static void main(String [] args){
		class InnerClass1{ // inner local class
		int a;
		int b;
		InnerClass1(int a,int b) {
			this.a=a;
			this.b=b;
		}
		int add1(){
			return a+b;
		}
		//static int diff(){return 3-4;}//Illegal static declaration in inner class InnerClass1
		
	}
		
		Classes t=new Classes(); 
		InnerClass2 ic2=t.new InnerClass2(10,20);
		InnerClass1 ic1=new InnerClass1(10,20); // local inner class and static class
		InnerClass3 ic3=new InnerClass3(10,20);
		System.out.println(ic1.add1());//System.out.println(ic1.diff());
		System.out.println(ic2.add2());
//System.out.println(ic2.diff()); non-static variable a cannot be referenced from a static context
		System.out.println(ic3.add3());
		System.out.println(ic3.diff());
	}
	class InnerClass2{ // inner class
		int a;
		int b;
		InnerClass2(int a,int b) {
			this.a=a;
			this.b=b;
		}
		int add2(){
			return a+b;
		}
		//static int diff(){ //Illegal static declaration in inner class Classes.InnerClass2
		//	return 3-4;
		//}
		
	}
	static class InnerClass3{ // inner static class .
		int a;
		int b;
		InnerClass3(int a,int b) {
			this.a=a;
			this.b=b;
		}
		int add3(){
			return a+b;
		}
		static int diff(){return 3-4;}
		
	}
	
}