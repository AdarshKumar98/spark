import java.util.*;
public class AbsoluteTargetDiff{
	public static void main(String [] args){
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		int target=sc.nextInt();
		int arr[] = new int[n];
		int counter=0;
		for(int i=0;i<n;i++)
			arr[i]=sc.nextInt();
		Set<Integer> value = new HashSet<Integer>();
		for(int i:arr)
			value.add(i);
		for(int i:value){
			if(value.contains(i+target))
				++counter;
		}
		System.out.println("set :"+value);
		Object [] objArray=value.toArray();
		for ( Object t:objArray)
			System.out.println(t);
		System.out.println(" no of equal differecences b/w numbers "+counter);
		
	}
}