name := "experian1"

version := "0.1"

scalaVersion := "2.11.8"
libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % "2.3.2",
  "org.apache.spark" %% "spark-sql" % "2.3.2",
  "org.scalatest" %% "scalatest" % "3.2.5" % "test" ,
  "org.scalatest" %% "scalatest" % "3.1.1",
  "com.github.mrpowers" %% "spark-fast-tests" % "0.23.0" % Test

)